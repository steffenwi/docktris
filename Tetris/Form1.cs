﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace Tetris
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        System.Media.SoundPlayer player = new System.Media.SoundPlayer();
        DatabaseWriter dbWriter = new DatabaseWriter("sa", "sa", @".\SQLEXPRESS", "Tetris");
        DatabaseReader dbReader = new DatabaseReader("sa", "sa", @".\SQLEXPRESS", "Tetris");
        bool paused = false;
        int stoneID = 0;
        int nextStoneID = 0;
        int secondsPassed = 0;
        int minutesPassed = 0;
        int level = 1;
        int lines = 0;
        bool level2Active = false;
        bool level3Active = false;
        bool level4Active = false;
        bool level5Active = false;
        bool level6Active = false;
        bool level7Active = false;
        bool level8Active = false;
        bool level9Active = false;
        bool level10Active = false;
        bool musicIsPlaying = true;
        bool scrollDown = true;
        bool validDatabaseConnection = true;
        int scrollHighscoreCount = 1;
        List<string> highscores = new List<string>();
        public Form1()
        {
            InitializeComponent();
            SetUpGame();
        }


        //Funktion um das Spiel in den startfähigen Zustand zu versetzen.
        public void SetUpGame()
        {
            txtPaused.BackColor = ColorTranslator.FromHtml("#ff7376");
            txtPaused.Visible = false;
            dataGridView1.Rows.Add(18);
            for (int i = 0; i < dataGridView1.Rows[18].Cells.Count; i++)
            {
                dataGridView1.Rows[18].Cells[i].Style.Tag = "ground";
                dataGridView1.Rows[18].Cells[i].Style.BackColor = Color.Gray;
            }

            int countHighscore = 1;
            int countPlayer = 1;

            highscores = dbReader.GetScores();
            for (int i = 0; i < highscores.Count; i++)
            {
                if(highscores.Count == 1)
                {
                    txtHighscore.Text = highscores[0];
                    validDatabaseConnection = false;
                    break;
                }
                if (countHighscore == 1)
                {
                    txtHighscore.Text += countPlayer.ToString() + " : " + highscores[i];
                    countHighscore = 2;
                }
                else if (countHighscore == 2)
                {
                    txtHighscore.Text += " - Punkte: " + highscores[i];
                    countHighscore = 3;
                }
                else if (countHighscore == 3)
                {
                    txtHighscore.Text += " - Zeit: " + highscores[i];
                    txtHighscore.AppendText(Environment.NewLine);
                    countHighscore = 1;
                    countPlayer++;
                }
            }


            player.Stream = Properties.Resources.Tetris;
            player.PlayLooping();


            nextStoneID = random.Next(1, 8);

            switch (nextStoneID)
            {
                case 1:
                    pbNext.Image = Properties.Resources.stone1;
                    break;
                case 2:
                    pbNext.Image = Properties.Resources.stone2;
                    break;
                case 3:
                    pbNext.Image = Properties.Resources.stone3;
                    break;
                case 4:
                    pbNext.Image = Properties.Resources.stone4;
                    break;
                case 5:
                    pbNext.Image = Properties.Resources.stone5;
                    break;
                case 6:
                    pbNext.Image = Properties.Resources.stone6;
                    break;
                case 7:
                    pbNext.Image = Properties.Resources.stone7;
                    break;
                default:
                    //Won't happen :^)
                    break;
            }
            stoneID = random.Next(1, 8);
            SpawnStone(stoneID);
            timerGame.Start();


        }

        //Timer welcher angibt, wann sich der aktuelle Block bewegt.
        private void timerGame_Tick(object sender, EventArgs e)
        {

            List<DataGridViewCell> cells = GetCurrentActiveBlock();
            if (CheckForBottom(cells))
            {
                for (int i = 0; i < cells.Count; i++)
                {
                    cells[i].Style.Tag = "ground";
                }
                int score = Convert.ToInt32(lblScore.Text);
                score = score + 5;
                lblScore.Text = score.ToString();
                timerGame.Stop();
                CheckForFullLine();
                stoneID = nextStoneID;
                nextStoneID = random.Next(1, 8);
                switch (nextStoneID)
                {
                    case 1:
                        pbNext.Image = Properties.Resources.stone1;
                        break;
                    case 2:
                        pbNext.Image = Properties.Resources.stone2;
                        break;
                    case 3:
                        pbNext.Image = Properties.Resources.stone3;
                        break;
                    case 4:
                        pbNext.Image = Properties.Resources.stone4;
                        break;
                    case 5:
                        pbNext.Image = Properties.Resources.stone5;
                        break;
                    case 6:
                        pbNext.Image = Properties.Resources.stone6;
                        break;
                    case 7:
                        pbNext.Image = Properties.Resources.stone7;
                        break;
                    default:
                        //Won't happen :^)
                        break;
                }
                SpawnStone(stoneID);
                timerGame.Start();
            }
            else
            {
                for (int i = 0; i < cells.Count; i++)
                {
                    cells[i].Style.BackColor = Color.White;
                    cells[i].Style.Tag = "";
                }
                for (int j = 0; j < cells.Count; j++)
                {
                    int row = cells[j].RowIndex;
                    int column = cells[j].ColumnIndex;
                    switch (stoneID)
                    {
                        case 1:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Green;
                            break;
                        case 2:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Black;
                            break;
                        case 3:
                            dataGridView1[column, row + 1].Style.BackColor = Color.PaleVioletRed;
                            break;
                        case 4:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Red;
                            break;
                        case 5:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Orange;
                            break;
                        case 6:
                            dataGridView1[column, row + 1].Style.BackColor = Color.MediumSeaGreen;
                            break;
                        case 7:
                            dataGridView1[column, row + 1].Style.BackColor = Color.SteelBlue;
                            break;
                    }
                    dataGridView1[column, row + 1].Style.Tag = "current";
                }
            }


        }

        public string CurrentTime()
        {
            string currentTime;

            secondsPassed++;

            if (secondsPassed % 60 == 0 && secondsPassed != 0)
            {
                secondsPassed = 0;
                minutesPassed++;
            }
            string minutesPassedAsString;
            string secondsPassedAsString;
            if (secondsPassed < 10)
            {
                secondsPassedAsString = "0" + secondsPassed.ToString();
            }
            else
            {
                secondsPassedAsString = secondsPassed.ToString();
            }

            if (minutesPassed < 10)
            {
                minutesPassedAsString = "0" + minutesPassed.ToString();
            }
            else
            {
                minutesPassedAsString = minutesPassed.ToString();
            }
            currentTime = minutesPassedAsString + ":" + secondsPassedAsString;

            return currentTime;
        }

        public bool CheckForBottom(List<DataGridViewCell> cells)
        {
            bool isOnGround = false;

            for (int i = 0; i < cells.Count; i++)
            {
                int row = cells[i].RowIndex;
                int column = cells[i].ColumnIndex;

                if (dataGridView1[column, row + 1].Style.Tag == "ground")
                {
                    isOnGround = true;
                }
            }

            return isOnGround;
        }


        public void CheckForFullLine()
        {
            //Check for full rows
            List<DataGridViewRow> rowsToDelete = new List<DataGridViewRow>();
            List<DataGridViewCell> cellsToDelete = new List<DataGridViewCell>();
            List<DataGridViewRow> countRowsToDrop = new List<DataGridViewRow>();
            int countRows = 0;
            int currentHighestRow = 30;

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                cellsToDelete.Clear();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Style.Tag == "ground" && cell.Style.BackColor != Color.Gray)
                    {
                        cellsToDelete.Add(cell);
                    }
                }
                if (cellsToDelete.Count == 10)
                {
                    rowsToDelete.Add(row);
                    countRowsToDrop.Add(row);
                }
            }

            //Score berechnung

            if (rowsToDelete.Count != 0)
            {
                int score = Convert.ToInt32(lblScore.Text);
                if (rowsToDelete.Count == 1)
                {
                    score = score + (40 * level);
                    lblScore.Text = score.ToString();
                }
                else if (rowsToDelete.Count == 2)
                {
                    score = score + (100 * level);
                    lblScore.Text = score.ToString();
                }
                else if (rowsToDelete.Count == 3)
                {
                    score = score + (300 * level);
                    lblScore.Text = score.ToString();
                }
                else if (rowsToDelete.Count == 4)
                {
                    score = score + (1200 * level);
                    lblScore.Text = score.ToString();
                }

                //Delete Rows

                for (int i = 0; i < rowsToDelete.Count; i++)
                {
                    List<DataGridViewCell> deleteCells = new List<DataGridViewCell>();
                    for (int j = 0; j < rowsToDelete[i].Cells.Count; j++)
                    {
                        deleteCells.Add(rowsToDelete[i].Cells[j]);
                    }

                    for (int k = 0; k < deleteCells.Count; k++)
                    {
                        deleteCells[k].Style.Tag = "delete";
                        deleteCells[k].Style.BackColor = Color.White;
                    }
                }

                //Rows nachrücken
                countRows = countRowsToDrop.Count;
                lines += countRows;
                lblLines.Text = lines.ToString();

                for (int i = 0; i < countRowsToDrop.Count; i++)
                {
                    if (countRowsToDrop[i].Index < currentHighestRow)
                    {
                        currentHighestRow = countRowsToDrop[i].Index;
                    }
                }

                List<int> lookForHighestRow = new List<int>();

                switch (countRowsToDrop.Count)
                {
                    case 1:
                        lookForHighestRow.Add(countRowsToDrop[0].Index);
                        break;
                    case 2:
                        lookForHighestRow.Add(countRowsToDrop[0].Index);
                        lookForHighestRow.Add(countRowsToDrop[1].Index);
                        break;
                    case 3:
                        lookForHighestRow.Add(countRowsToDrop[0].Index);
                        lookForHighestRow.Add(countRowsToDrop[1].Index);
                        lookForHighestRow.Add(countRowsToDrop[2].Index);
                        break;
                    case 4:
                        lookForHighestRow.Add(countRowsToDrop[0].Index);
                        lookForHighestRow.Add(countRowsToDrop[1].Index);
                        lookForHighestRow.Add(countRowsToDrop[2].Index);
                        lookForHighestRow.Add(countRowsToDrop[3].Index);
                        break;
                }

                //lookForHighestRow[0] ist ab hier die höchste Reihe.
                lookForHighestRow.Sort();

                int row1 = 1000;
                int row2 = 1000;
                int row3 = 1000;
                int row4 = 1000;

                Dictionary<string, int> rowDict = new Dictionary<string, int>();
                Dictionary<string, int> columnDict = new Dictionary<string, int>();

                for (int i = dataGridView1.Rows.Count - 1; i >= 0; i--)
                {
                    if (dataGridView1.Rows[i].Index == 18)
                    {
                        //Hier passiert nichts, das es sich um den Boden des Spielfelds handelt.
                    }
                    else if (dataGridView1.Rows[i].Index < 18)
                    {
                        for (int j = 0; j < lookForHighestRow.Count; j++)
                        {
                            if (dataGridView1.Rows[i].Index == lookForHighestRow[j])
                            {
                                rowDict.Add(string.Format("row{0}", (j + 1).ToString()), dataGridView1.Rows[i].Index);
                            }
                        }
                    }
                }

                if (lookForHighestRow.Count == 1)
                {
                    row1 = rowDict["row1"];
                }
                else if (lookForHighestRow.Count == 2)
                {
                    row1 = rowDict["row1"];
                    row2 = rowDict["row2"];
                }
                else if (lookForHighestRow.Count == 3)
                {
                    row1 = rowDict["row1"];
                    row2 = rowDict["row2"];
                    row3 = rowDict["row3"];
                }
                else if (lookForHighestRow.Count == 4)
                {
                    row1 = rowDict["row1"];
                    row2 = rowDict["row2"];
                    row3 = rowDict["row3"];
                    row4 = rowDict["row4"];
                }


                for (int i = dataGridView1.Rows.Count - 1; i >= 0; i--)
                {
                    if (dataGridView1.Rows[i].Index < 17 && dataGridView1.Rows[i].Index > row1)
                    {
                        foreach (DataGridViewCell cell in dataGridView1.Rows[i].Cells)
                        {
                            for (int j = 0; j < dataGridView1.Rows[i + 1].Cells.Count; j++)
                            {
                                if (dataGridView1.Rows[i + 1].Cells[j].Style.Tag == "delete")
                                {
                                    int row = cell.RowIndex;
                                    int column = cell.ColumnIndex;

                                    dataGridView1[column, row + 1].Style.Tag = dataGridView1[column, row].Style.Tag;
                                    dataGridView1[column, row + 1].Style.BackColor = dataGridView1[column, row].Style.BackColor;
                                }
                            }
                        }
                    }
                    else if (dataGridView1.Rows[i].Index < 17 && dataGridView1.Rows[i].Index < row1)
                    {
                        foreach (DataGridViewCell cell in dataGridView1.Rows[i].Cells)
                        {
                            int row = cell.RowIndex;
                            int column = cell.ColumnIndex;

                            dataGridView1[column, row + countRows].Style.Tag = dataGridView1[column, row].Style.Tag;
                            dataGridView1[column, row + countRows].Style.BackColor = dataGridView1[column, row].Style.BackColor;
                        }
                    }
                }
            }

        }


        public void GameOver()
        {
            timerGame.Stop();
            timerGame.Enabled = false;
            timerGame.Dispose();
            timerTime.Stop();
            for (int i = 0; i < GetCurrentActiveBlock().Count; i++)
            {
                GetCurrentActiveBlock()[i].Style.Tag = "ground";
            }
            player.Stop();
            txtName.Visible = true;
            btnSend.Visible = true;
            btnExit.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;

            txtName.BringToFront();
            btnSend.BringToFront();
            btnExit.BringToFront();

        }

        public void SpawnStone(int stoneID)
        {
            switch (stoneID)
            {
                case 1:
                    if (dataGridView1[5, 0].Style.Tag == "ground" || dataGridView1[5, 1].Style.Tag == "ground" || dataGridView1[5, 2].Style.Tag == "ground" || dataGridView1[5, 3].Style.Tag == "ground")
                    {
                        GameOver();
                    }
                    else
                    {
                        dataGridView1[5, 0].Style.BackColor = Color.Green;
                        dataGridView1[5, 0].Style.Tag = "current";
                        dataGridView1[5, 1].Style.BackColor = Color.Green;
                        dataGridView1[5, 1].Style.Tag = "current";
                        dataGridView1[5, 2].Style.BackColor = Color.Green;
                        dataGridView1[5, 2].Style.Tag = "current";
                        dataGridView1[5, 3].Style.BackColor = Color.Green;
                        dataGridView1[5, 3].Style.Tag = "current";
                        break;
                    }
                    break;

                case 2:
                    if (dataGridView1[4, 0].Style.Tag == "ground" || dataGridView1[5, 0].Style.Tag == "ground" || dataGridView1[4, 1].Style.Tag == "ground" || dataGridView1[5, 1].Style.Tag == "ground")
                    {
                        GameOver();
                        break;
                    }
                    else
                    {
                        dataGridView1[4, 0].Style.BackColor = Color.Black;
                        dataGridView1[4, 0].Style.Tag = "current";
                        dataGridView1[5, 0].Style.BackColor = Color.Black;
                        dataGridView1[5, 0].Style.Tag = "current";
                        dataGridView1[4, 1].Style.BackColor = Color.Black;
                        dataGridView1[4, 1].Style.Tag = "current";
                        dataGridView1[5, 1].Style.BackColor = Color.Black;
                        dataGridView1[5, 1].Style.Tag = "current";
                        break;
                    }

                case 3:
                    if (dataGridView1[5, 0].Style.Tag == "ground" || dataGridView1[4, 1].Style.Tag == "ground" || dataGridView1[5, 1].Style.Tag == "ground" || dataGridView1[6, 1].Style.Tag == "ground")
                    {
                        GameOver();
                        break;
                    }
                    else
                    {
                        dataGridView1[5, 0].Style.BackColor = Color.PaleVioletRed;
                        dataGridView1[5, 0].Style.Tag = "current";
                        dataGridView1[4, 1].Style.BackColor = Color.PaleVioletRed;
                        dataGridView1[4, 1].Style.Tag = "current";
                        dataGridView1[5, 1].Style.BackColor = Color.PaleVioletRed;
                        dataGridView1[5, 1].Style.Tag = "current";
                        dataGridView1[6, 1].Style.BackColor = Color.PaleVioletRed;
                        dataGridView1[6, 1].Style.Tag = "current";
                        break;
                    }

                case 4:
                    if (dataGridView1[5, 0].Style.Tag == "ground" || dataGridView1[6, 0].Style.Tag == "ground" || dataGridView1[4, 1].Style.Tag == "ground" || dataGridView1[5, 1].Style.Tag == "ground")
                    {
                        GameOver();
                        break;
                    }
                    else
                    {
                        dataGridView1[5, 0].Style.BackColor = Color.Red;
                        dataGridView1[5, 0].Style.Tag = "current";
                        dataGridView1[6, 0].Style.BackColor = Color.Red;
                        dataGridView1[6, 0].Style.Tag = "current";
                        dataGridView1[4, 1].Style.BackColor = Color.Red;
                        dataGridView1[4, 1].Style.Tag = "current";
                        dataGridView1[5, 1].Style.BackColor = Color.Red;
                        dataGridView1[5, 1].Style.Tag = "current";
                        break;
                    }

                case 5:
                    if (dataGridView1[4, 0].Style.Tag == "ground" || dataGridView1[5, 0].Style.Tag == "ground" || dataGridView1[5, 1].Style.Tag == "ground" || dataGridView1[6, 1].Style.Tag == "ground")
                    {
                        GameOver();
                        break;
                    }
                    else
                    {
                        dataGridView1[4, 0].Style.BackColor = Color.Orange;
                        dataGridView1[4, 0].Style.Tag = "current";
                        dataGridView1[5, 0].Style.BackColor = Color.Orange;
                        dataGridView1[5, 0].Style.Tag = "current";
                        dataGridView1[5, 1].Style.BackColor = Color.Orange;
                        dataGridView1[5, 1].Style.Tag = "current";
                        dataGridView1[6, 1].Style.BackColor = Color.Orange;
                        dataGridView1[6, 1].Style.Tag = "current";
                        break;
                    }

                case 6:
                    if (dataGridView1[5, 0].Style.Tag == "ground" || dataGridView1[5, 1].Style.Tag == "ground" || dataGridView1[5, 1].Style.Tag == "ground" || dataGridView1[6, 2].Style.Tag == "ground")
                    {
                        GameOver();
                        break;
                    }
                    else
                    {
                        dataGridView1[5, 0].Style.BackColor = Color.MediumSeaGreen;
                        dataGridView1[5, 0].Style.Tag = "current";
                        dataGridView1[5, 1].Style.BackColor = Color.MediumSeaGreen;
                        dataGridView1[5, 1].Style.Tag = "current";
                        dataGridView1[5, 2].Style.BackColor = Color.MediumSeaGreen;
                        dataGridView1[5, 2].Style.Tag = "current";
                        dataGridView1[6, 2].Style.BackColor = Color.MediumSeaGreen;
                        dataGridView1[6, 2].Style.Tag = "current";
                        break;
                    }

                case 7:
                    if (dataGridView1[6, 0].Style.Tag == "ground" || dataGridView1[6, 1].Style.Tag == "ground" || dataGridView1[6, 2].Style.Tag == "ground" || dataGridView1[5, 2].Style.Tag == "ground")
                    {
                        GameOver();
                        break;
                    }
                    else
                    {
                        dataGridView1[6, 0].Style.BackColor = Color.SteelBlue;
                        dataGridView1[6, 0].Style.Tag = "current";
                        dataGridView1[6, 1].Style.BackColor = Color.SteelBlue;
                        dataGridView1[6, 1].Style.Tag = "current";
                        dataGridView1[6, 2].Style.BackColor = Color.SteelBlue;
                        dataGridView1[6, 2].Style.Tag = "current";
                        dataGridView1[5, 2].Style.BackColor = Color.SteelBlue;
                        dataGridView1[5, 2].Style.Tag = "current";
                        break;
                    }

                default:
                    Exception ex = new Exception();
                    throw ex;
            }

        }

        public void MoveLeft(int stoneID)
        {
            bool possibleToMove = true;
            List<DataGridViewCell> cellsToMove = GetCurrentActiveBlock();


            for (int k = 0; k < cellsToMove.Count; k++)
            {
                if (cellsToMove[k].ColumnIndex == 0 || dataGridView1[cellsToMove[k].ColumnIndex - 1, cellsToMove[k].RowIndex].Style.Tag == "ground")
                {
                    possibleToMove = false;
                    break;
                }
            }
            if (possibleToMove)
            {
                for (int i = 0; i < cellsToMove.Count; i++)
                {
                    cellsToMove[i].Style.BackColor = Color.White;
                    cellsToMove[i].Style.Tag = "";
                }
                for (int j = 0; j < cellsToMove.Count; j++)
                {
                    int row = cellsToMove[j].RowIndex;
                    int column = cellsToMove[j].ColumnIndex;
                    switch (stoneID)
                    {
                        case 1:
                            dataGridView1[column - 1, row].Style.BackColor = Color.Green;
                            break;
                        case 2:
                            dataGridView1[column - 1, row].Style.BackColor = Color.Black;
                            break;
                        case 3:
                            dataGridView1[column - 1, row].Style.BackColor = Color.PaleVioletRed;
                            break;
                        case 4:
                            dataGridView1[column - 1, row].Style.BackColor = Color.Red;
                            break;
                        case 5:
                            dataGridView1[column - 1, row].Style.BackColor = Color.Orange;
                            break;
                        case 6:
                            dataGridView1[column - 1, row].Style.BackColor = Color.MediumSeaGreen;
                            break;
                        case 7:
                            dataGridView1[column - 1, row].Style.BackColor = Color.SteelBlue;
                            break;
                    }
                    dataGridView1[column - 1, row].Style.Tag = "current";
                }
            }

        }
        public void MoveRight(int stoneID)
        {
            bool possibleToMove = true;
            List<DataGridViewCell> cellsToMove = GetCurrentActiveBlock();

            for (int k = 0; k < cellsToMove.Count; k++)
            {
                if (cellsToMove[k].ColumnIndex == 9 || dataGridView1[cellsToMove[k].ColumnIndex + 1, cellsToMove[k].RowIndex].Style.Tag == "ground")
                {
                    possibleToMove = false;
                    break;
                }
            }

            if (possibleToMove)
            {
                for (int i = 0; i < cellsToMove.Count; i++)
                {
                    cellsToMove[i].Style.BackColor = Color.White;
                    cellsToMove[i].Style.Tag = "";
                }
                for (int j = 0; j < cellsToMove.Count; j++)
                {
                    int row = cellsToMove[j].RowIndex;
                    int column = cellsToMove[j].ColumnIndex;
                    switch (stoneID)
                    {
                        case 1:
                            dataGridView1[column + 1, row].Style.BackColor = Color.Green;
                            break;
                        case 2:
                            dataGridView1[column + 1, row].Style.BackColor = Color.Black;
                            break;
                        case 3:
                            dataGridView1[column + 1, row].Style.BackColor = Color.PaleVioletRed;
                            break;
                        case 4:
                            dataGridView1[column + 1, row].Style.BackColor = Color.Red;
                            break;
                        case 5:
                            dataGridView1[column + 1, row].Style.BackColor = Color.Orange;
                            break;
                        case 6:
                            dataGridView1[column + 1, row].Style.BackColor = Color.MediumSeaGreen;
                            break;
                        case 7:
                            dataGridView1[column + 1, row].Style.BackColor = Color.SteelBlue;
                            break;
                    }
                    dataGridView1[column + 1, row].Style.Tag = "current";
                }
            }

        }

        public void Turn(int stoneID)
        {
            List<DataGridViewCell> cellsToMove = GetCurrentActiveBlock();
            List<DataGridViewCell> blocksToCheck = new List<DataGridViewCell>();

            switch (stoneID)
            {
                case 1:


                    if (cellsToMove[0].ColumnIndex == cellsToMove[1].ColumnIndex && cellsToMove[0].ColumnIndex < 8 && cellsToMove[0].ColumnIndex > 0)
                    {
                        //Von Senkrecht auf Waagerecht

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex - 1, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex + 1, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex + 2, cellsToMove[3].RowIndex]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }



                            dataGridView1[cellsToMove[0].ColumnIndex - 1, cellsToMove[3].RowIndex].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[0].ColumnIndex - 1, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[0].ColumnIndex + 1, cellsToMove[3].RowIndex].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[0].ColumnIndex + 1, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[0].ColumnIndex + 2, cellsToMove[3].RowIndex].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[0].ColumnIndex + 2, cellsToMove[3].RowIndex].Style.Tag = "current";
                        }



                    }
                    else if (cellsToMove[0].RowIndex == cellsToMove[1].RowIndex && cellsToMove[0].ColumnIndex >= 0)
                    {
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[0].RowIndex - 3]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 2]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[2].RowIndex - 1]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex]);
                        //Stein liegt waagerecht und soll auf senkrecht gedreht werden
                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[0].RowIndex - 3].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[0].RowIndex - 3].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 2].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 2].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[2].RowIndex - 1].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[2].RowIndex - 1].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.Green;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                        }
                    }

                    break;
                case 2:
                    //2x2
                    //Ist in jeder Form gleich, bleibt also wie es ist.
                    break;
                case 3:
                    //Treppe

                    if (cellsToMove[1].RowIndex == cellsToMove[2].RowIndex && cellsToMove[2].RowIndex == cellsToMove[3].RowIndex && cellsToMove[2].RowIndex < 17)
                    {
                        //State 1 
                        //   x
                        //x  x   x

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex + 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex + 1].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex + 1].Style.Tag = "current";
                        }


                    }
                    else if (cellsToMove[0].ColumnIndex == cellsToMove[2].ColumnIndex && cellsToMove[2].ColumnIndex == cellsToMove[3].ColumnIndex && cellsToMove[0].ColumnIndex < 9)
                    {
                        //State 2
                        //   x
                        //x  x
                        //   x
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex + 1, cellsToMove[2].RowIndex]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex + 1, cellsToMove[2].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[2].ColumnIndex + 1, cellsToMove[2].RowIndex].Style.Tag = "current";
                        }


                    }
                    else if (cellsToMove[0].RowIndex == cellsToMove[1].RowIndex && cellsToMove[1].RowIndex == cellsToMove[2].RowIndex && cellsToMove[0].RowIndex > 0)
                    {
                        //State 3
                        // x  x  x
                        //    x

                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 1].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 1].Style.Tag = "current";
                        }

                    }
                    else if (cellsToMove[0].ColumnIndex == cellsToMove[1].ColumnIndex && cellsToMove[1].ColumnIndex == cellsToMove[3].ColumnIndex && cellsToMove[0].ColumnIndex > 0)
                    {
                        //State 4
                        //  x
                        //  x  x
                        //  x

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex - 1, cellsToMove[1].RowIndex]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex - 1, cellsToMove[1].RowIndex].Style.BackColor = Color.PaleVioletRed;
                            dataGridView1[cellsToMove[1].ColumnIndex - 1, cellsToMove[1].RowIndex].Style.Tag = "current";
                        }

                    }

                    break;
                case 4:
                    //S

                    if (cellsToMove[0].RowIndex == cellsToMove[1].RowIndex && cellsToMove[2].RowIndex == cellsToMove[3].RowIndex && cellsToMove[3].RowIndex < 16)
                    {
                        //State 1
                        //    x  x
                        // x  x

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex + 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex + 1].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex + 1].Style.Tag = "current";
                        }


                    }

                    else if (cellsToMove[0].ColumnIndex == cellsToMove[1].ColumnIndex && cellsToMove[2].ColumnIndex == cellsToMove[3].ColumnIndex && cellsToMove[1].ColumnIndex > 1)
                    {
                        //State 2
                        // x
                        // x  x
                        //    x
                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex + 1, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex - 1, cellsToMove[1].RowIndex]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[0].ColumnIndex + 1, cellsToMove[0].RowIndex].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[0].ColumnIndex + 1, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex - 1, cellsToMove[1].RowIndex].Style.BackColor = Color.Red;
                            dataGridView1[cellsToMove[1].ColumnIndex - 1, cellsToMove[1].RowIndex].Style.Tag = "current";
                        }


                    }


                    break;
                case 5:
                    //Z




                    if (cellsToMove[0].RowIndex == cellsToMove[1].RowIndex && cellsToMove[2].RowIndex == cellsToMove[3].RowIndex && cellsToMove[0].RowIndex > 0)
                    {
                        //State 1
                        // x  x
                        //    x  x

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex + 1]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex + 1].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex + 1].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 1].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex - 1].Style.Tag = "current";
                        }



                    }
                    else if (cellsToMove[0].ColumnIndex == cellsToMove[2].ColumnIndex && cellsToMove[1].ColumnIndex == cellsToMove[3].ColumnIndex && cellsToMove[2].ColumnIndex < 9)
                    {
                        //State 2
                        //     x
                        //  x  x
                        //  x

                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex + 1]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex + 1, cellsToMove[2].RowIndex + 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex + 1].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex + 1].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex + 1, cellsToMove[2].RowIndex + 1].Style.BackColor = Color.Orange;
                            dataGridView1[cellsToMove[2].ColumnIndex + 1, cellsToMove[2].RowIndex + 1].Style.Tag = "current";
                        }
                    }



                    break;
                case 6:
                    //L



                    if (cellsToMove[0].ColumnIndex == cellsToMove[1].ColumnIndex && cellsToMove[1].ColumnIndex == cellsToMove[2].ColumnIndex && cellsToMove[3].ColumnIndex < 9)
                    {
                        //State 1
                        // x
                        // x
                        // x x

                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex - 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex - 1].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex - 1].Style.Tag = "current";
                        }



                    }
                    else if (cellsToMove[1].RowIndex == cellsToMove[2].RowIndex && cellsToMove[2].RowIndex == cellsToMove[3].RowIndex && cellsToMove[2].RowIndex > 0)
                    {
                        //State 2
                        //       x
                        // x  x  x

                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex - 1]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex - 2]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[0].RowIndex - 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex - 1].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex - 1].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex - 2].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex - 2].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[0].RowIndex - 1].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[0].RowIndex - 1].Style.Tag = "current";
                        }



                    }
                    else if (cellsToMove[1].ColumnIndex == cellsToMove[2].ColumnIndex && cellsToMove[2].ColumnIndex == cellsToMove[3].ColumnIndex && cellsToMove[1].ColumnIndex < 9)
                    {
                        //State 3
                        // x  x
                        //    x
                        //    x

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex + 1, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex + 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex + 1, cellsToMove[1].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[1].ColumnIndex + 1, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex + 1].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex + 1].Style.Tag = "current";
                        }


                    }
                    else if (cellsToMove[0].RowIndex == cellsToMove[1].RowIndex && cellsToMove[1].RowIndex == cellsToMove[2].RowIndex && cellsToMove[3].RowIndex < 17)
                    {
                        //State 4
                        // x  x  x
                        // x

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex + 1]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex + 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex + 1].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex + 1].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex + 1].Style.BackColor = Color.MediumSeaGreen;
                            dataGridView1[cellsToMove[3].ColumnIndex + 1, cellsToMove[3].RowIndex + 1].Style.Tag = "current";
                        }

                    }

                    break;
                case 7:
                    //Reverse L

                    if (cellsToMove[0].ColumnIndex == cellsToMove[1].ColumnIndex && cellsToMove[1].ColumnIndex == cellsToMove[3].ColumnIndex && cellsToMove[2].ColumnIndex > 0)
                    {
                        //State 1
                        //    x
                        //    x
                        // x  x 
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex - 1, cellsToMove[1].RowIndex]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[3].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex - 1, cellsToMove[1].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[2].ColumnIndex - 1, cellsToMove[1].RowIndex].Style.Tag = "current";
                        }


                    }
                    else if (cellsToMove[0].RowIndex == cellsToMove[1].RowIndex && cellsToMove[1].RowIndex == cellsToMove[2].RowIndex && cellsToMove[3].RowIndex < 17)
                    {
                        //State 2
                        // x  x  x
                        //       x

                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex + 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex + 1].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[3].RowIndex + 1].Style.Tag = "current";
                        }


                    }
                    else if (cellsToMove[0].ColumnIndex == cellsToMove[2].ColumnIndex && cellsToMove[2].ColumnIndex == cellsToMove[3].ColumnIndex && cellsToMove[1].ColumnIndex < 9)
                    {
                        //State 3
                        // x  x
                        // x
                        // x

                        blocksToCheck.Add(dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex + 1, cellsToMove[2].RowIndex]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[0].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[1].ColumnIndex + 1, cellsToMove[2].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[1].ColumnIndex + 1, cellsToMove[2].RowIndex].Style.Tag = "current";
                        }


                    }
                    else if (cellsToMove[1].RowIndex == cellsToMove[2].RowIndex && cellsToMove[2].RowIndex == cellsToMove[3].RowIndex && cellsToMove[0].RowIndex > 0)
                    {
                        //State 4
                        // x
                        // x  x  x

                        blocksToCheck.Add(dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[0].RowIndex]);
                        blocksToCheck.Add(dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[0].RowIndex - 1]);

                        if (CheckIfTurnIsPossible(blocksToCheck))
                        {
                            for (int i = 0; i < cellsToMove.Count; i++)
                            {
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.BackColor = Color.White;
                                dataGridView1[cellsToMove[i].ColumnIndex, cellsToMove[i].RowIndex].Style.Tag = "";
                            }

                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[1].ColumnIndex, cellsToMove[1].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[2].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[0].RowIndex].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[0].RowIndex].Style.Tag = "current";
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[0].RowIndex - 1].Style.BackColor = Color.SteelBlue;
                            dataGridView1[cellsToMove[2].ColumnIndex, cellsToMove[0].RowIndex - 1].Style.Tag = "current";
                        }

                    }

                    break;
            }

        }

        public bool CheckIfTurnIsPossible(List<DataGridViewCell> blockCells)
        {
            bool turnIsValid = true;
            for (int i = 0; i < blockCells.Count; i++)
            {
                int row = blockCells[i].RowIndex;
                int column = blockCells[i].ColumnIndex;

                if (dataGridView1[column, row].Style.Tag == "ground")
                {
                    turnIsValid = false;
                }
                else
                {
                    turnIsValid = true;
                }
            }

            return turnIsValid;
        }
        public void MoveDown(int stoneID)
        {
            bool possibleToMove = true;
            List<DataGridViewCell> cellsToMove = GetCurrentActiveBlock();

            for (int k = 0; k < cellsToMove.Count; k++)
            {
                int row = cellsToMove[k].RowIndex;
                int column = cellsToMove[k].ColumnIndex;

                if (dataGridView1[column, row + 1].Style.Tag == "ground" || row == 17)
                {
                    possibleToMove = false;
                    break;
                }
            }

            if (possibleToMove)
            {
                for (int i = 0; i < cellsToMove.Count; i++)
                {
                    cellsToMove[i].Style.BackColor = Color.White;
                    cellsToMove[i].Style.Tag = "";
                }
                for (int j = 0; j < cellsToMove.Count; j++)
                {
                    int row = cellsToMove[j].RowIndex;
                    int column = cellsToMove[j].ColumnIndex;
                    switch (stoneID)
                    {
                        case 1:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Green;
                            break;
                        case 2:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Black;
                            break;
                        case 3:
                            dataGridView1[column, row + 1].Style.BackColor = Color.PaleVioletRed;
                            break;
                        case 4:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Red;
                            break;
                        case 5:
                            dataGridView1[column, row + 1].Style.BackColor = Color.Orange;
                            break;
                        case 6:
                            dataGridView1[column, row + 1].Style.BackColor = Color.MediumSeaGreen;
                            break;
                        case 7:
                            dataGridView1[column, row + 1].Style.BackColor = Color.SteelBlue;
                            break;
                    }
                    dataGridView1[column, row + 1].Style.Tag = "current";
                }
            }
        }


        public List<DataGridViewCell> GetCurrentActiveBlock()
        {
            List<DataGridViewCell> currentBlock = new List<DataGridViewCell>();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Style.Tag == "current")
                    {
                        currentBlock.Add(cell);
                    }
                }
            }

            return currentBlock;
        }




        //Sorgt dafür, dass keine Zelle selected werden kann
        private void dataGridView1_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell == null || e.StateChanged != DataGridViewElementStates.Selected)
                return;
            else
                e.Cell.Selected = false;
        }

        public void PauseResume()
        {
            if (paused)
            {
                txtPaused.Visible = false;
                pausedTimer.Stop();
                paused = false;
                timerGame.Start();
                timerTime.Start();
            }
            else
            {
                txtPaused.Visible = true;
                pausedTimer.Start();
                paused = true;
                timerGame.Stop();
                timerTime.Stop();
            }

        }


        //Eventhandler um den aktuellen Stein zu bewegen.
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Left:
                    MoveLeft(stoneID);
                    break;
                case Keys.Right:
                    MoveRight(stoneID);
                    break;
                case Keys.Up:
                    Turn(stoneID);
                    break;
                case Keys.Down:
                    MoveDown(stoneID);
                    break;
                case Keys.Escape:
                    PauseResume();
                    break;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (musicIsPlaying)
            {
                pictureBox1.Image = Properties.Resources.soundOFF;
                musicIsPlaying = false;
                player.Stop();
            }
            else if (!musicIsPlaying)
            {
                pictureBox1.Image = Properties.Resources.soundON;
                musicIsPlaying = true;
                player.Play();
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string score = lblScore.Text;
            string timePassed = lblTime.Text;
            string name = txtName.Text;
            if(validDatabaseConnection)
            {
                dbWriter.WriteHighscore(name, score, timePassed);
            }
            else
            {
                MessageBox.Show("Es kann keine Verbindung zur Datenbank\n aufgebaut werden, das Spiel wird sich nun schließen!", "Datenbank-Fehler");
            }

            Application.Exit();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timerTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = CurrentTime();

            switch (lines)
            {
                case 15:

                    if (!level2Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level2Active = true;
                        lblLevel.Text = "2";
                    }

                    break;
                case 16:

                    if (!level2Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level2Active = true;
                        lblLevel.Text = "2";
                    }
                    break;
                case 17:

                    if (!level2Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level2Active = true;
                        lblLevel.Text = "2";
                    }
                    break;
                case 18:
                    if (!level2Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level2Active = true;
                        lblLevel.Text = "2";
                    }
                    break;
                case 19:
                    if (!level2Active)
                    {
                        timerGame.Interval -= 90;
                        level++;
                        level2Active = true;
                        lblLevel.Text = "2";
                    }

                    break;
                case 25:

                    if (!level3Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level3Active = true;
                        lblLevel.Text = "3";
                    }

                    break;
                case 26:
                    if (!level3Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level3Active = true;
                        lblLevel.Text = "3";
                    }

                    break;

                case 27:
                    if (!level3Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level3Active = true;
                        lblLevel.Text = "3";
                    }

                    break;
                case 28:
                    if (!level3Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level3Active = true;
                        lblLevel.Text = "3";
                    }

                    break;
                case 29:
                    if (!level3Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level3Active = true;
                        lblLevel.Text = "3";
                    }

                    break;
                case 35:

                    if (!level4Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level4Active = true;
                        lblLevel.Text = "4";
                    }

                    break;
                case 36:
                    if (!level4Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level4Active = true;
                        lblLevel.Text = "4";
                    }

                    break;
                case 37:
                    if (!level4Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level4Active = true;
                        lblLevel.Text = "4";
                    }

                    break;
                case 38:
                    if (!level4Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level4Active = true;
                        lblLevel.Text = "4";
                    }

                    break;
                case 39:
                    if (!level4Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level4Active = true;
                        lblLevel.Text = "4";
                    }

                    break;

                case 45:

                    if (!level5Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level5Active = true;
                        lblLevel.Text = "5";
                    }

                    break;

                case 46:
                    if (!level5Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level5Active = true;
                        lblLevel.Text = "5";
                    }

                    break;
                case 47:
                    if (!level5Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level5Active = true;
                        lblLevel.Text = "5";
                    }

                    break;
                case 48:
                    if (!level5Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level5Active = true;
                        lblLevel.Text = "5";
                    }

                    break;
                case 49:
                    if (!level5Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level5Active = true;
                        lblLevel.Text = "5";
                    }

                    break;
                case 55:

                    if (!level6Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level6Active = true;
                        lblLevel.Text = "6";
                    }

                    break;
                case 56:
                    if (!level6Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level6Active = true;
                        lblLevel.Text = "6";
                    }

                    break;
                case 57:
                    if (!level6Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level6Active = true;
                        lblLevel.Text = "6";
                    }

                    break;
                case 58:
                    if (!level6Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level6Active = true;
                        lblLevel.Text = "6";
                    }

                    break;
                case 59:
                    if (!level6Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level6Active = true;
                        lblLevel.Text = "6";
                    }

                    break;

                case 65:

                    if (!level7Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level7Active = true;
                        lblLevel.Text = "7";
                    }

                    break;
                case 66:
                    if (!level7Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level7Active = true;
                        lblLevel.Text = "7";
                    }

                    break;
                case 67:
                    if (!level7Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level7Active = true;
                        lblLevel.Text = "7";
                    }

                    break;
                case 68:
                    if (!level7Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level7Active = true;
                        lblLevel.Text = "7";
                    }

                    break;
                case 69:
                    if (!level7Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level7Active = true;
                        lblLevel.Text = "7";
                    }

                    break;

                case 75:

                    if (!level8Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level8Active = true;
                        lblLevel.Text = "8";
                    }

                    break;
                case 76:
                    if (!level8Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level8Active = true;
                        lblLevel.Text = "8";
                    }

                    break;
                case 77:
                    if (!level8Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level8Active = true;
                        lblLevel.Text = "8";
                    }

                    break;
                case 78:
                    if (!level8Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level8Active = true;
                        lblLevel.Text = "8";
                    }

                    break;
                case 79:
                    if (!level8Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level8Active = true;
                        lblLevel.Text = "8";
                    }

                    break;

                case 85:

                    if (!level9Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level9Active = true;
                        lblLevel.Text = "9";
                    }

                    break;
                case 86:
                    if (!level9Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level9Active = true;
                        lblLevel.Text = "9";
                    }

                    break;
                case 87:
                    if (!level9Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level9Active = true;
                        lblLevel.Text = "9";
                    }

                    break;
                case 88:
                    if (!level9Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level9Active = true;
                        lblLevel.Text = "9";
                    }

                    break;
                case 89:
                    if (!level9Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level9Active = true;
                        lblLevel.Text = "9";
                    }

                    break;

                case 95:

                    if (!level10Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level10Active = true;
                        lblLevel.Text = "10";
                        lblLevel.Font = new Font(lblLevel.Font, FontStyle.Bold);
                    }

                    break;
                case 96:
                    if (!level10Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level10Active = true;
                        lblLevel.Text = "10";
                        lblLevel.Font = new Font(lblLevel.Font, FontStyle.Bold);
                    }

                    break;
                case 97:
                    if (!level10Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level10Active = true;
                        lblLevel.Text = "10";
                        lblLevel.Font = new Font(lblLevel.Font, FontStyle.Bold);
                    }

                    break;
                case 98:
                    if (!level10Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level10Active = true;
                        lblLevel.Text = "10";
                        lblLevel.Font = new Font(lblLevel.Font, FontStyle.Bold);
                    }

                    break;
                case 99:
                    if (!level10Active)
                    {
                        level++;
                        timerGame.Interval -= 90;
                        level10Active = true;
                        lblLevel.Text = "10";
                        lblLevel.Font = new Font(lblLevel.Font, FontStyle.Bold);
                        lblLevel.ForeColor = Color.Red;
                    }

                    break;
                default:
                    //Max level
                    break;
            }
        }

        private void timerHighscore_Tick(object sender, EventArgs e)
        {
            if (highscores.Count > 3)
            {
                int datastrings = highscores.Count / 3;

                if (scrollDown)
                {
                    txtHighscore.SelectionStart = txtHighscore.TextLength / datastrings * scrollHighscoreCount;
                    txtHighscore.SelectionLength = 1;
                    txtHighscore.ScrollToCaret();

                    scrollHighscoreCount++;

                    if (scrollHighscoreCount >= datastrings)
                    {
                        scrollDown = false;
                        scrollHighscoreCount = 1;
                    }
                }
                else
                {
                    txtHighscore.SelectionStart = 0;
                    txtHighscore.SelectionLength = 1;
                    txtHighscore.ScrollToCaret();
                    scrollDown = true;
                }
            }

        }

        private void pausedTimer_Tick(object sender, EventArgs e)
        {
            if (txtPaused.Top > 104 && txtPaused.Tag == "")
            {
                txtPaused.Top -= 2;
            }
            else
            {
                txtPaused.Tag = "1";
            }

            if (txtPaused.Top + txtPaused.Height < 526 && txtPaused.Tag == "1")
            {
                txtPaused.Top += 2;
            }
            else
            {
                txtPaused.Tag = "";
            }

        }
    }
}