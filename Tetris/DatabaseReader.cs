﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Tetris
{
    class DatabaseReader
    {
        private string connectionString;
        SqlConnectionStringBuilder b = new SqlConnectionStringBuilder();

        public DatabaseReader(string username, string password, string server, string database)
        {
                b.UserID = username;
                b.Password = password;
                b.InitialCatalog = database;
                b.DataSource = server;

                this.connectionString = b.ConnectionString;
        }

        public List<string> GetScores()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(this.connectionString))
                {
                    SqlCommand getScores = new SqlCommand("Select name, score, time FROM dbo.highscores ORDER BY score Desc", con);
                    SqlDataAdapter da = new SqlDataAdapter(getScores);
                    DataTable data = new DataTable();
                    da.Fill(data);

                    List<string> highscores = new List<string>();
                    foreach (DataRow row in data.Rows)
                    {
                        //Name
                        highscores.Add(row[0].ToString());
                        //Score
                        highscores.Add(row[1].ToString());
                        //Zeit
                        highscores.Add(row[2].ToString());
                    }

                    return highscores;
                }
            }
            catch(Exception ex)
            {
                List<string> ErrorList = new List<string>();
                ErrorList.Add("Datenbankverbindungsfehler!");
                return ErrorList;
            }
            
        }

    }
}
