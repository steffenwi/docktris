﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Tetris
{
    class DatabaseWriter
    {
        private string connectionString;
        SqlConnectionStringBuilder b = new SqlConnectionStringBuilder();

        public DatabaseWriter(string username, string password, string server, string database)
        {
            b.UserID = username;
            b.Password = password;
            b.InitialCatalog = database;
            b.DataSource = server;

            this.connectionString = b.ConnectionString;
        }

        public void WriteHighscore(string name, string score, string zeit)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                SqlCommand writeHighscore = new SqlCommand("Insert into dbo.highscores (name, time, score) VALUES (@Name, @Time, @Score)", con);
                writeHighscore.Parameters.AddWithValue("@Name", name);
                writeHighscore.Parameters.AddWithValue("@Time", zeit);
                writeHighscore.Parameters.AddWithValue("@Score", score);

                con.Open();
                writeHighscore.ExecuteNonQuery();
            }
        }
    }
}
